# Sublime Text 3 Settings and Keymaps

This repository contains custom settings for Sublime Text 3.
It is used for synchronization between different devices and for upgrading to new settings.

## Usage

- Install Sublime Text 3.
- Open Sublime Text 3 and use the command palette to install Package Control.
- Close Sublime Text 3.

- Execute the following commands in the shell.
```
cd ~/.config/sublime-text-3/Packages
rm -r User
git clone https://github.com/lyrahgames/sublime_text_3-config.git User
```

- Restart Sublime Text 3 and wait.
